#!/bin/bash

#
# In this directory...
#
DIR=/var/log/apache2

#
# ...search for files with this string in the file name...
#
FILENAME_SEARCH=other_vhosts_access
FILES=$(sudo ls -t $DIR | grep $FILENAME_SEARCH)

#
# ...and count the number of lines in each file.
#
for FILE in $FILES
do
        F=$DIR/$FILE
        if [ "${F: -3}" == ".gz" ]
        then
                COUNT=$(sudo zcat $F | wc -l)
                HITS="$F :: hits =  $COUNT"
                echo $HITS
        else
                COUNT=$(sudo cat $F | wc -l)
                HITS="$F :: hits =  $COUNT"
                echo $HITS
        fi
done
